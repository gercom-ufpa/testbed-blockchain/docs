# Começando com Minifabric!
Este documento descreve o processo de como fazer o seguinte:
* Instalar o Minifabric em uma máquina;
* Configurar dois sites, um é considerado uma rede Fabric existente, o outro é considerado uma nova organização;
* Adicionar uma nova organização à rede Fabric existente;
* Afiliar um par de nova organização ao um canal existente;
* Instalar o chainconde em execução em pares já existentes em novos pares;
* Aprovar e persistir o chaincode;
* Verificar os containers de endosso e chaincode;
* Limpar uma rede Fabric com Minifabric


## Prerequisites

* **Docker CE 18.03** ou mais recente.

## Levantando uma rede Fabric
Suponha que haja dois diretórios de trabalho, `site_A` e` site_B` no diretório raiz. Cada diretório de trabalho representará um site que pode incluir uma ou várias organizações e pares, os dois diretórios de trabalho podem muito bem estar em servidores diferentes; nesse caso, você precisará ter outros meios, como o protocolo de cópia segura `scp`, para transferir os arquivos necessários entre os servidores.

No cenário apresentado nesse tutorial serão utilizados duas máquinas conectadas a mesma rede, mas ele pode ser executado na mesma máquina, substituindo as cópias utilizando `scp` por `cp` entre os diretórios.

Para mais informações acesse o [repositório oficial do Minifabric](https://github.com/hyperledger-labs/minifabric).

### Instalando o Minifabric
 Para instalar o Minifabric usando Linux (Ubuntu, Fedora, CentOS) ou OS X.
```
curl -o minifab -sL https://tinyurl.com/yxa2q6yr && chmod +x minifab && sudo mv minifab /usr/local/bin/
```
O primeiro comando irá obter o script do Minifabric, gerando um arquivo de nome minifab, enquanto que o segundo  concede a este permissão de execução. Além disso, com o terceiro comando, este arquivo é movido para um caminho no PATH, permitindo a execução do Minifabric de qualquer lugar do sistema.

### Configurando a primeira organização e levantando a rede
No primeiro computador, criaremos e entraremos no diretório de trabalho com o camando a seguir.
```
mkdir site_A && cd site_A
```
Use seu editor de texto para criar o arquivo das especificações da rede.
```
editor spec.yaml
```
Copie e salve as seguintes informações no arquivo `spec.yaml`.
```
fabric:
  cas:
  - "ca1.org1.com.br"
  peers:
  - "peer1.org1.com.br"
  - "peer1.org2.com.br"
  orderers:
  - "orderer1.org0.com.br"
  settings:
    ca:
      FABRIC_LOGGING_SPEC: DEBUG
    peer:
      FABRIC_LOGGING_SPEC: DEBUG
    orderer:
      FABRIC_LOGGING_SPEC: DEBUG
  ### use go proxy when default go proxy is restricted in some of the regions.
  ### the default goproxy
  # goproxy: "https://proxy.golang.org,direct"
  ### the goproxy in China area
  # goproxy: "https://goproxy.cn,direct"
  ### set the endpoint address to override the automatically detected IP address
  # endpoint_address: 104.196.45.144
  ### set the docker network name to override the automatically generated name.
  netname: "site_A"
  ### set the extra optins for docker run command
  # container_options: "--restart=always --log-opt max-size=10m --log-opt max-file=3"
```

Para Levantar a rede use o comando a seguir.
```
sudo minifab up -o org1.com.br -e 7050 -s couchdb -n samplecc -p ''
```
Esse comando levantará uma rede contendo as especificações do arquivo `spec.yaml`. Uma rede Fabric completa com um canal denominado `mychannel`, um chaincode denominado `samplecc` instalado, aprovado, confirmado e inicializado, e um banco de dados `couchdb` para persistência de dados. Existem duas organizações de mesmo nível e uma organização de pedidos. Assim que o comando for concluído com êxito, uma rede Fabric totalmente em execução é criada e considere-a como uma rede Fabric existente.

### Configurando a nova organização

Crie um diretório e entre no diretório para configurar a nova organização. Caso esteja usando máquinas diferentes deverá [instalar o Minifabric](#instalando-o-minifabric) novamente.
```
mkdir site_B && cd site_B
```
Crie o documento das especificações da nova organização:
```
editor spec.yaml
```
Copie as seguintes especificações (`spec.yaml`) da nova organização.
```
fabric:
  cas:
  - "ca1.org3.com.br"
  peers:
  - "peer1.org3.com.br"
  settings:
    ca:
      FABRIC_LOGGING_SPEC: DEBUG
    peer:
      FABRIC_LOGGING_SPEC: DEBUG
  ### use go proxy when default go proxy is restricted in some of the regions.
  ### the default goproxy
  # goproxy: "https://proxy.golang.org,direct"
  ### the goproxy in China area
  # goproxy: "https://goproxy.cn,direct"
  ### set the endpoint address to override the automatically detected IP address
  # endpoint_address: 104.196.45.144
  ### set the docker network name to override the automatically generated name.
  netname: "site_B"
  ### set the extra optins for docker run command
  # container_options: "--restart=always --log-opt max-size=10m --log-opt max-file=3"
```
Levante a rede com o seguinte comando.
```
sudo minifab netup -o org3.com.br
```
Esse comando, diferente da primeira organização, levanta um organização com apenas um par e uma autoridade certificadora. Além disso, nenhum canal e nenhum nó de pedido é criado, como também, não há a instalação de nenhum chaincode.

## Adicionando uma nova organização à rede Fabric existente

Uma vez configurado a nova organização, o arquivo produzido pelo Minifabric para ingressar em uma rede existente será denominado `JoinRequest_org3-example-com.json` no diretório `~/site_B/vars`. Se você tiver um nome diferente para sua organização, o arquivo de solicitação de adesão terá um nome de arquivo diferente, faça as alterações de acordo ao executar os comandos. Além disso, deve substituir o `USER` e o `IP_SERVER_A` pelo respectivo nome de usuário e ip do do computador da primeira organização. Envie a requisição de ingresso na rede para o diretório da primeira organização com o comando abaixo.
```
scp vars/JoinRequest_org3-com-br.json USER@IP_SERVER_A:/home/USER/site_A/vars/NewOrgJoinRequest.json
```
No computador da primeira organização, aprove a solicitação da nova organização com o comando abaixo.
```
sudo minifab orgjoin
```

### Importando os nós de pedidos para a nova organização e adicionando os pares dela para o canal
Para que os pares na nova organização participem da rede do Fabric, a organização deve saber onde estão os nós do ordenador. Para fazer isso, copiamos o arquivo`endpoints` e utilizamos o `nodeimport`.
```
scp vars/profiles/endpoints.yaml USER@IP_SERVER_B:/home/USER/site_B/vars
minifab nodeimport,join
```
A partir desse ponto a nova organização já fará parte da rede e do canal formado pela primeira organização.

## Instalando e aprovando o chaincode na nova organização
Após a nova organização se juntar a rede Fabric, para que haja o consenso, durante o processo de leitura e atualização do ledger, é necessário instalar e aprovar o mesmo chaincode que foi instalado nos pares da primeira organização.
```
minifab install,approve -n samplecc -p ''
```

### Aprovando o chaincode pela primeira organização
Se novas organizações ingressaram, o chaincode precisará ser aprovado novamente para que a nova organização também possa confirmar
```
minifab approve,discover,commit
```

### Descubrindo e verificando o chaincode na nova organização

```
minifab discover
```
Verifique se o arquivo `./vars/discover/mychannel/samplecc_endorsers.json` contém a nova organização como grupo de endosso.
```
minifab stats
```
O comando acima deve mostrar que a organização deve ter um contêiner chaincode como o seguinte em execução.
```
  dev-peer1.org3.example.com-samplecc_1.0-9ea5e3809f : Up 4 minutes
```

### Testando o chaincode

Em qualquer uma das organizações é possível invocar o chaincode e fazer uma escrita em uma variável com o comando a seguir:
```
sudo minifab invoke -n samplecc -p '"invoke","put","b","10"'
```
É possível verificar a escrita no outro site com o seguinte comando:
```
sudo minifab invoke -n samplecc -p '"invoke","get","b"'
```
O comando deve mostrar algo como o seguinte:

```
['\x1b[34m2021-07-31 22:20:41.224 UTC [chaincodeCmd] chaincodeInvokeOrQuery -> INFO 001\x1b[0m Chaincode invoke successful. result: status:200 payload:"10" ']
```

## Limpando a rede
Você pode usar um dos dois comandos abaixo para desligar a rede Fabric.
```
cd PASTA_SITE
sudo minifab down && sudo minifab cleanup
```
O primeiro comando simplesmente remove todos os contêineres docker que compõem a rede Fabric, ele NÃO removerá nenhum certificado ou dado de razão, você pode executar o `minifab netup` mais tarde para reiniciar tudo incluindo contêineres chaincode se houver algum. O segundo comando remove todos os contêineres e limpa o diretório de trabalho.

Se necessário, também é possível remover todos os arquivos gerados durante o levantamento da rede, na pasta da rede, use o seguinte comando.
```
sudo rm -rf vars
```
